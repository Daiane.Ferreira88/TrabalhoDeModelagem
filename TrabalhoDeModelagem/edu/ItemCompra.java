package br.sistema.edu;


public class ItemCompra {
	
	
	private float preco;
	private String descricao;
	private float quantidadeItem;
	
	
	private Produto produto = new Produto();
	
	public ItemCompra(String descricao, float preco,float quantidade) {
			this.preco = preco;
			this.descricao = descricao;
			this.quantidadeItem = quantidade;
	}

	private float getPreco() {
		return preco;
	}

	private void setPreco(float preco) {
		this.preco = preco;
	}

	String getDescricao() {
		return descricao;
	}

	private void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	float getQuantidadeItem1() {
		return quantidadeItem;
	}

	private void setQuantidadeItem(int quantidadeItem) {
		quantidadeItem = quantidadeItem;
	}

	public int getValor() {
		// TODO Auto-generated method stub
		return 0;
	}

	public int getQuantidadeItem() {
	
		return 0;
	}

}
