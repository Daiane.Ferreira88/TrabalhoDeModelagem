package br.sistema.edu;

public class Endereco {
	
	private String rua;
	private int numero;
	private String cep;
	private String cidade;
	private String uf;
	private TipoEndereco tipo;
	
	
	public Endereco(String rua, String cep, String cidade, String uf, int numero, TipoEndereco tipo) {
		this.rua = rua;
		this.cep = cep;
		this.cidade = cidade;
		this.uf = uf;
		this.numero = numero;
		this.tipo = tipo;
}
	
	private String getRua() {
		return rua;
	}


	private void setRua(String rua) {
		this.rua = rua;
	}


	private int getNumero() {
		return numero;
	}




	private void setNumero(int numero) {
		this.numero = numero;
	}

	
	private String getCep() {
		return cep;
	}

	private void setCep(String cep) {
		this.cep = cep;
	}
	

	private String getCidade() {
		return cidade;

	}

   private void setCidade(String cidade) {
		this.cidade = cidade;
	
   }

    private String getUf() {
		return uf;
	
    }


	private void setUf(String uf) {
		this.uf = uf;
	}

}

